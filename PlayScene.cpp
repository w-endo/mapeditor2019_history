﻿#include "PlayScene.h"
#include "Map.h"


//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{
	CreateGameObject<Map>(this);
	
}

//更新
void PlayScene::Update()
{

}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}