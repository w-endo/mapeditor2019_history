﻿#pragma once
#include "Engine/IGameObject.h"


//視点を管理するクラス
class Controller : public IGameObject
{
	//視点操作
	void ViewpointOperation();

	D3DXVECTOR3 selectPos_;

public:
	//コンストラクタ
	Controller(IGameObject* parent);

	//デストラクタ
	~Controller();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;



	//描画
	void Draw() override;

	//開放
	void Release() override;
};