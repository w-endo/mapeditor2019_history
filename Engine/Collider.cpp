#include "Collider.h"



Collider::Collider(IGameObject* owner, D3DXVECTOR3 center, float radius)
{
	owner_ = owner;
	center_ = center;
	radius_ = radius;
}


Collider::~Collider()
{
}

bool Collider::IsHit(Collider& target)
{
	D3DXVECTOR3 v = (center_+ owner_->GetPosition()) - 
		(target.center_ + target.owner_->GetPosition());

	float length = D3DXVec3Length(&v);
	if (length <= radius_ + target.radius_)
	{
		return true;
	}

	return false;
}
