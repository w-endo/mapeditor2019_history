﻿#include "Quad.h"
#include "Direct3D.h"

Quad::Quad():pVertexBuffer_(nullptr), pIndexBuffer_(nullptr), pTexture_(nullptr), material_({ 0 })
{
}

Quad::~Quad()
{
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);
}

void Quad::Load(const char* fileName)
{
	//頂点情報
	Vertex vertexList[] = 
	{
		D3DXVECTOR3(-1, 1, 0),  D3DXVECTOR3(0, 0, -1),D3DXVECTOR2(0, 0),
		D3DXVECTOR3(1, 1, 0),   D3DXVECTOR3(0, 0, -1),D3DXVECTOR2(1, 0),
		D3DXVECTOR3(1, -1, 0),  D3DXVECTOR3(0, 0, -1),D3DXVECTOR2(1, 1),
		D3DXVECTOR3(-1, -1, 0), D3DXVECTOR3(0, 0, -1),D3DXVECTOR2(0, 1),
	};

	//頂点バッファへ
	Direct3D::pDevice->CreateVertexBuffer(sizeof(vertexList), 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED, &pVertexBuffer_, 0);
	assert(pVertexBuffer_ != nullptr);

	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(vertexList));
	pVertexBuffer_->Unlock();

	//インデックス情報
	int indexList[] = { 0, 2, 3, 0, 1, 2 };

	//インデックスバッファへ
	Direct3D::pDevice->CreateIndexBuffer(sizeof(indexList), 0, D3DFMT_INDEX32,
		D3DPOOL_MANAGED, &pIndexBuffer_, 0);
	assert(pIndexBuffer_ != nullptr);
	DWORD *iCopy;
	pIndexBuffer_->Lock(0, 0, (void**)&iCopy, 0);
	memcpy(iCopy, indexList, sizeof(indexList));
	pIndexBuffer_->Unlock();

	//テクスチャ作成
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, fileName,
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);
	assert(pTexture_ != nullptr);

	//マテリアルの設定
	material_.Diffuse.r = 1.0f;
	material_.Diffuse.g = 1.0f;
	material_.Diffuse.b = 1.0f;
}

void Quad::Draw(const D3DXMATRIX &matrix)
{
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);
	Direct3D::pDevice->SetTexture(0, pTexture_);
	Direct3D::pDevice->SetMaterial(&material_);
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));
	Direct3D::pDevice->SetIndices(pIndexBuffer_);
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);
	Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
}
