﻿#pragma once
#include "Global.h"

namespace Direct3D
{
	extern LPDIRECT3D9			pD3d;	   //Direct3Dオブジェクト
	extern LPDIRECT3DDEVICE9	pDevice;	//Direct3Dデバイスオブジェクト

	//Direct3Dの初期化
	//引数：hWnd	ウィンドウハンドル
	//戻値：なし
	void Initialize(HWND hWnd);

	//描画開始
	//引数：なし
	//戻値：なし
	void BeginDraw();

	void EndDraw();

	void Release();
}