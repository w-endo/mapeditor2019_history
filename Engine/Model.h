﻿#include <string>
#include "Fbx.h"
//H
namespace Model
{
	struct ModelData
	{
		std::string fileName;
		Fbx*	pFbx;
		D3DXMATRIX matrix;

		ModelData() : fileName(""), pFbx(nullptr)
		{
			D3DXMatrixIdentity(&matrix);
		}
	};

	int Load(std::string fileName);
	void Draw(int handle);
	void SetMatrix(int handle, D3DXMATRIX& matrix);
	void Release(int handle);
	void AllRelease();
	void RayCast(int handle, RayCastData& data);
};