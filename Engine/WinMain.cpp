﻿#include <Windows.h>
#include "Global.h"
#include "Direct3D.h"
#include "RootJob.h"
#include "Model.h"
#include "../resource.h"
#include "../Map.h"
#include "../PlayScene.h"

#ifdef _DEBUG
#include <crtdbg.h>
#endif


//リンカ
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")

//定数
const char* WIN_CLASS_NAME = "SampleGame";	//ウィンドウクラス名
const int	WINDOW_WIDTH = 800;	 //ウィンドウの幅
const int	WINDOW_HEIGHT = 600;	//ウィンドウの高さ

//プロトタイプ宣言
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp);

//グローバル変数
Global g;
RootJob *pRootJob;

//エントリーポイント
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
#ifdef _DEBUG
	// メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif


	HWND hDlg = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), nullptr, (DLGPROC)DialogProc);



	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);	            //この構造体のサイズ
	wc.hInstance = hInstance;	                  //インスタンスハンドル
	wc.lpszClassName = WIN_CLASS_NAME;	        	  //ウィンドウクラス名
	wc.lpfnWndProc = WndProc;	                  //ウィンドウプロシージャ
	wc.style = CS_VREDRAW | CS_HREDRAW;	        //スタイル（デフォルト）
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);	//アイコン
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);	  //小さいアイコン
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);	  //マウスカーソル
	wc.lpszMenuName = NULL;	                    //メニュー（なし）
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	//背景（白）
	RegisterClassEx(&wc);	//クラスを登録

	RECT winRect = { 0, 0, WINDOW_WIDTH , WINDOW_HEIGHT };
	AdjustWindowRect(&winRect, WS_CHILD, FALSE);


	//ウィンドウを作成
	HWND hWnd = CreateWindow(
		WIN_CLASS_NAME,	       //ウィンドウクラス名
		"サンプルゲーム",    	//タイトルバーに表示する内容
		WS_CHILD,				//スタイル（普通のウィンドウ）
		0,	      //表示位置左（おまかせ）
		0,	      //表示位置上（おまかせ）
		winRect.right - winRect.left,	       //ウィンドウ幅
		winRect.bottom - winRect.top,	       //ウィンドウ高さ
		hDlg,	               //親ウインドウ
		NULL,	               //メニュー（なし）
		hInstance,	          //インスタンス
		NULL	                //パラメータ（なし）
	);
	assert(hWnd != NULL);

	//ウィンドウを表示
	ShowWindow(hWnd, nCmdShow);

	g.screenWidth = WINDOW_WIDTH;
	g.screenHeight = WINDOW_HEIGHT;

	//Direct3Dの初期化（準備）
	Direct3D::Initialize(hWnd);

	//DirectInputの初期化
	Input::Initialize(hWnd);

	pRootJob = new RootJob;
	pRootJob->Initialize();

	//メッセージループ（何か起きるのを待つ）
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	while (msg.message != WM_QUIT)
	{
		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし
		else
		{
			//ゲームの処理

		   //入力情報の更新
			Input::Update();
			pRootJob->UpdateSub();

			Direct3D::BeginDraw();

			pRootJob->DrawSub();

			Direct3D::EndDraw();
		}

	}

	pRootJob->ReleaseSub();
	SAFE_DELETE(pRootJob);
	Input::Release();
	Direct3D::Release();
	Model::AllRelease();

	return 0;

}

//ウィンドウプロシージャ（何かあった時によばれる関数）
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);	//プログラム終了
		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

//ダイアログプロシージャ
BOOL CALLBACK DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	switch (msg)
	{
	case WM_INITDIALOG:
		SendMessage(GetDlgItem(hDlg, IDC_RADIO_UP),	BM_SETCHECK, BST_CHECKED, 0);

		SendMessage(GetDlgItem(hDlg, IDC_COMBO2), CB_INSERTSTRING, 0, (LPARAM)"デフォルト");
		SendMessage(GetDlgItem(hDlg, IDC_COMBO2), CB_INSERTSTRING, 1, (LPARAM)"草原");
		SendMessage(GetDlgItem(hDlg, IDC_COMBO2), CB_INSERTSTRING, 2, (LPARAM)"砂地");
		SendMessage(GetDlgItem(hDlg, IDC_COMBO2), CB_INSERTSTRING, 3, (LPARAM)"レンガ");
		SendMessage(GetDlgItem(hDlg, IDC_COMBO2), CB_INSERTSTRING, 4, (LPARAM)"水");
		SendMessage(GetDlgItem(hDlg, IDC_COMBO2), CB_SETCURSEL, 0, 0);
		return TRUE;

	case WM_CLOSE:
		PostQuitMessage(0);
		return TRUE;

	case WM_COMMAND:
		return ((Map*)SceneManager::GetCurrentScene()->
			GetChild(0))->CommandProcess(hDlg, wp);


	}
	return FALSE;
}