﻿#include <assert.h>
#include <d3dx9.h>
#include "Input.h"


namespace Input
{
	//DirectInput本体
	LPDIRECTINPUT8   pDInput = nullptr;

	//キーボード
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr; //デバイスオブジェクト
	BYTE keyState[256] = { 0 };     //各キーの状態
	BYTE prevKeyState[256] = { 0 };    //前フレームでの各キーの状態

	//マウス
	LPDIRECTINPUTDEVICE8 pMouseDevice; //デバイスオブジェクト
	DIMOUSESTATE mouseState;    //マウスの状態
	DIMOUSESTATE prevMouseState;   //前フレームのマウスの状態

	//ウィンドウハンドル
	HWND hWnd_;

	//初期化処理
	void Initialize(HWND hWnd)
	{
		DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&pDInput, nullptr);
		assert(pDInput != nullptr);

		//キーボード
		{
			pDInput->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr);
			assert(pKeyDevice);
			pKeyDevice->SetDataFormat(&c_dfDIKeyboard);
			pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
		}

		//マウス
		{
			pDInput->CreateDevice(GUID_SysMouse, &pMouseDevice, nullptr);
			assert(pKeyDevice);
			pMouseDevice->SetDataFormat(&c_dfDIMouse);
			pMouseDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
		}

		hWnd_ = hWnd;
	}

	//更新処理
	void Update()
	{
		//キーボード
		memcpy(prevKeyState, keyState, sizeof(keyState));
		pKeyDevice->Acquire();
		pKeyDevice->GetDeviceState(sizeof(keyState), &keyState);

		//マウス
		memcpy(&prevMouseState, &mouseState, sizeof(mouseState));
		pMouseDevice->Acquire();
		pMouseDevice->GetDeviceState(sizeof(mouseState), &mouseState);

	}

	//解放処理
	void Release()
	{
		SAFE_RELEASE(pMouseDevice);
		SAFE_RELEASE(pKeyDevice);
		SAFE_RELEASE(pDInput);
	}


	///////////////////////////// キーボード情報取得 //////////////////////////////////


	bool IsKey(int keyCode)
	{
		if (keyState[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	bool IsKeyDown(int keyCode)
	{
		//今は押してて、前回は押してない
		if (IsKey(keyCode) &&
			(prevKeyState[keyCode] & 0x80) == 0)
		{
			return true;
		}
		return false;
	}

	bool IsKeyUp(int keyCode)
	{
		//今は押してて、前回は押してない
		if (IsKey(keyCode) == false &&
			prevKeyState[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}


	///////////////////////////// マウス情報取得 //////////////////////////////////

	//マウスのボタンが押されているか調べる
	bool IsMouseButton(int buttonCode)
	{
		//押してる
		if (mouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今押したか調べる（押しっぱなしは無効）
	bool IsMouseButtonDown(int buttonCode)
	{
		//今は押してて、前回は押してない
		if (IsMouseButton(buttonCode) && !(prevMouseState.rgbButtons[buttonCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今放したか調べる
	bool IsMouseButtonUp(int buttonCode)
	{
		//今押してなくて、前回は押してる
		if (!IsMouseButton(buttonCode) && prevMouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスカーソルの位置を取得
	D3DXVECTOR3 GetMousePosition()
	{
		POINT mousePos;
		GetCursorPos(&mousePos);
		ScreenToClient(hWnd_, &mousePos);

		D3DXVECTOR3 result = D3DXVECTOR3(mousePos.x, mousePos.y, 0);
		return result;
	}


	//そのフレームでのマウスの移動量を取得
	D3DXVECTOR3 GetMouseMove()
	{
		D3DXVECTOR3 result(mouseState.lX, mouseState.lY, mouseState.lZ);
		return result;
	}
}