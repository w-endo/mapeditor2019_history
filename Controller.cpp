﻿#include "Controller.h"
#include "Engine/Camera.h"
#include "Engine/Input.h"
#include "Engine/Direct3D.h"
#include "Engine/Model.h"
#include "Map.h"

//コンストラクタ
Controller::Controller(IGameObject * parent)
	:IGameObject(parent, "Controller")
{
}

//デストラクタ
Controller::~Controller()
{
}

//初期化
void Controller::Initialize()
{
	//初期位置をステージの中央にする
	position_ = D3DXVECTOR3(MAP_SIZE / 2, 0, MAP_SIZE / 2);

	//子オブジェクトとしてカメラを作成
	Camera* pCamera_ = CreateGameObject<Camera>(this);
	pCamera_->SetPosition(D3DXVECTOR3(0, 0, -10.0f));

}

//更新
void Controller::Update()
{
	//視点操作
	ViewpointOperation();


	//マウス操作
	D3DXVECTOR3 mousePosNear = Input::GetMousePosition();
	D3DXVECTOR3 mousePosFar = Input::GetMousePosition();
	mousePosFar.z = 1;

	//ビュー行列
	D3DXMATRIX view;
	Direct3D::pDevice->GetTransform(D3DTS_VIEW, &view);

	//プロジェクション行列
	D3DXMATRIX proj;
	Direct3D::pDevice->GetTransform(D3DTS_PROJECTION, &proj);

	//ビューポート行列
	D3DXMATRIX vp;
	D3DXMatrixIdentity(&vp);
	vp._11 = g.screenWidth / 2.0f;
	vp._22 = -g.screenHeight / 2.0f;
	vp._41 = g.screenWidth / 2.0f;
	vp._42 = g.screenHeight / 2.0f;

	//それぞれの逆行列
	D3DXMATRIX invView, invProj, invVp;
	D3DXMatrixInverse(&invView, nullptr, &view);
	D3DXMatrixInverse(&invProj, nullptr, &proj);
	D3DXMatrixInverse(&invVp, nullptr, &vp);

	//逆行列の合成
	D3DXMATRIX invTransform = invVp * invProj * invView;

	//マウスの位置を3次元へ
	D3DXVec3TransformCoord(&mousePosNear, &mousePosNear, &invTransform);
	D3DXVec3TransformCoord(&mousePosFar, &mousePosFar, &invTransform);

	selectPos_ = ((Map*)pParent_)->GetMouseCoord(
		mousePosNear, mousePosFar);



	if (Input::IsMouseButtonDown(0))
	{
		((Map*)pParent_)->Edit(selectPos_.x, selectPos_.z);
	}
}

//視点操作
void Controller::ViewpointOperation()
{
	//視点移動	//回転に合わせた移動させなきゃいけない
	{
		//回転行列を用意
		D3DXMATRIX matRot;
		D3DXMatrixRotationY(&matRot, D3DXToRadian(rotate_.y));

		D3DXVECTOR3 vecForward = D3DXVECTOR3(0, 0, 0.1f);			//奥移動ベクトル
		D3DXVec3TransformCoord(&vecForward, &vecForward, &matRot);

		D3DXVECTOR3 vecRight = D3DXVECTOR3(0.1f, 0, 0);				//右移動ベクトル
		D3DXVec3TransformCoord(&vecRight, &vecRight, &matRot);

		//移動
		if (Input::IsKey(DIK_A))	position_ -= vecRight;
		if (Input::IsKey(DIK_D))	position_ += vecRight;
		if (Input::IsKey(DIK_W))	position_ += vecForward;
		if (Input::IsKey(DIK_S))	position_ -= vecForward;

		//はみ出ないように
		if (position_.x < 0)		position_.x = 0;
		if (position_.x > MAP_SIZE)	position_.x = MAP_SIZE;
		if (position_.z < 0)		position_.z = 0;
		if (position_.z > MAP_SIZE)	position_.z = MAP_SIZE;
	}

	//視点の回転
	{
		if (Input::IsKey(DIK_UP))		rotate_.x += 1.0f;
		if (Input::IsKey(DIK_DOWN))		rotate_.x -= 1.0f;
		if (Input::IsKey(DIK_LEFT))		rotate_.y += 1.0f;
		if (Input::IsKey(DIK_RIGHT))	rotate_.y -= 1.0f;

		//制限
		if (rotate_.x >= 90)	rotate_.x = 89.9f;
		if (rotate_.x < 0)	rotate_.x = 0;
	}
}

//描画
void Controller::Draw()
{

}

//開放
void Controller::Release()
{
}

