﻿#include <assert.h>
#include "Map.h"
#include "Engine/Model.h"
#include "Controller.h"
#include "resource.h"

//コンストラクタ
Map::Map(IGameObject * parent)
	:IGameObject(parent, "Map"), editType_(0), selectType_(0)
{
}

//デストラクタ
Map::~Map()
{
}

//初期化
void Map::Initialize()
{
	//マップデータの初期化
	for (int x = 0; x < MAP_SIZE; x++)
	{
		for (int z = 0; z < MAP_SIZE; z++)
		{
			table_[x][z].type = 0;		//種類は0番
			table_[x][z].height = 1;	//高さは1
		}
	}

	//FBXファイルのロード
	std::string fileName[] =
	{
		"Data/BoxDefault.fbx" ,
		"Data/BoxGrass.fbx",
		"Data/BoxSand.fbx",
		"Data/BoxBrick.fbx",
		"Data/BoxWater.fbx"
	};
	for (int i = 0; i < 5; i++)
	{
		hModel_[i] = Model::Load(fileName[i]);
		assert(hModel_[i] > -1);
	}

	CreateGameObject<Controller>(this);
}

//更新
void Map::Update()
{
}

//描画
void Map::Draw()
{
	//各区画を２重ループで
	for (int x = 0; x < MAP_SIZE; x++)
	{
		for (int z = 0; z < MAP_SIZE; z++)
		{
			//その区画の高さ分、さらにループ
			for (int y = 0; y < table_[x][z].height; y++)
			{
				//移動行列作成
				D3DXMATRIX matTrans;
				D3DXMatrixTranslation(&matTrans, (float)x, (float)y, (float)z);

				//その区画の種類　＝　表示するFBXファイル
				int type = table_[x][z].type;

				//描画
				Model::SetMatrix(hModel_[type], matTrans);
				Model::Draw(hModel_[type]);
			}
		}
	}
}

//開放
void Map::Release()
{
}

void Map::Edit(int x, int z)
{
	if (x < 0)	return;

	switch (editType_)
	{
	case 0:
		table_[x][z].height++;
		break;

	case 1:
		table_[x][z].height--;
		break;

	case 2:
		table_[x][z].type = selectType_;
		break;
	}

}

D3DXVECTOR3 Map::GetMouseCoord(D3DXVECTOR3 mousePosNear, D3DXVECTOR3 mousePosFar)
{
	D3DXVECTOR3 result = D3DXVECTOR3(-999, 0, 0);

	RayCastData data;
	data.start = mousePosNear;
	data.direction = mousePosFar - mousePosNear;

	float miniDistance = 9999.0f;

	for (int x = 0; x < MAP_SIZE; x++)
	{
		for (int z = 0; z < MAP_SIZE; z++)
		{
			for (int y = 0; y < table_[x][z].height; y++)
			{

				D3DXMATRIX matTrans;
				D3DXMatrixTranslation(&matTrans,
					(float)x, (float)y, (float)z);
				Model::SetMatrix(hModel_[table_[x][z].type], matTrans);

				Model::RayCast(table_[x][z].type, data);
				if (data.hit)
				{
					if (data.distance < miniDistance)
					{
						result.x = x;
						result.z = z;
						miniDistance = data.distance;
					}
				}
			}
		}
	}
	return result;
}


BOOL Map::CommandProcess(HWND hDlg, WPARAM wp)
{
	switch (LOWORD(wp))
	{
	case IDC_RADIO_UP:
		editType_ = 0;
		return TRUE;

	case IDC_RADIO_DOWN:
		editType_ = 1;
		return TRUE;

	case IDC_RADIO_CHANGE:
		editType_ = 2;
		return TRUE;

	case IDC_COMBO2:
		selectType_ = (int)SendMessage(GetDlgItem(hDlg, IDC_COMBO2), CB_GETCURSEL, 0, 0);
		return TRUE;

	case ID_MENU_SAVE:
		Save();
		return TRUE;
	}

	return FALSE;
}

void Map::Save()
{
	char fileName[MAX_PATH] = "無題.map";  //ファイル名を入れる変数

	//「ファイルを保存」ダイアログの設定
	OPENFILENAME ofn;                         //名前をつけて保存ダイアログの設定用構造体
	ZeroMemory(&ofn, sizeof(ofn));            //構造体初期化
	ofn.lStructSize = sizeof(OPENFILENAME);   //構造体のサイズ
	ofn.lpstrFilter = TEXT("マップデータ(*.map)\0*.map\0")        //─┬ファイルの種類
					  TEXT("すべてのファイル(*.*)\0*.*\0\0");     //─┘
	ofn.lpstrFile = fileName;               //ファイル名
	ofn.nMaxFile = MAX_PATH;               //パスの最大文字数
	ofn.Flags = OFN_OVERWRITEPROMPT;     //フラグ（同名ファイルが存在したら上書き確認）
	ofn.lpstrDefExt = "map";                  //デフォルト拡張子

	//「ファイルを保存」ダイアログ
	BOOL selFile;
	selFile = GetSaveFileName(&ofn);

	//キャンセルしたら中断
	if (selFile == FALSE) return;



	HANDLE hFile;        //ファイルのハンドル
	hFile = CreateFile(
		fileName,                 //ファイル名
		GENERIC_WRITE,           //アクセスモード（書き込み用）
		0,                      //共有（なし）
		NULL,                   //セキュリティ属性（継承しない）
		CREATE_ALWAYS,           //作成方法
		FILE_ATTRIBUTE_NORMAL,  //属性とフラグ（設定なし）
		NULL);                  //拡張属性（なし）



	DWORD dwBytes = 0;  //書き込み位置

	for (int x = 0; x < MAP_SIZE; x++)
	{
		for (int z = 0; z < MAP_SIZE; z++)
		{
			char data[8];
			wsprintf(data, "%d,%d,", table_[x][z].height, table_[x][z].type);
			WriteFile(
				hFile,                   //ファイルハンドル
				data,                  //保存するデータ（文字列）
				(DWORD)strlen(data),   //書き込む文字数
				&dwBytes,                //書き込んだサイズを入れる変数
				NULL);                   //オーバーラップド構造体（今回は使わない）
		}
	}

	CloseHandle(hFile);
}
