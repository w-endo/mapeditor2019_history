﻿#pragma once
#include "Engine/IGameObject.h"

//マップの幅＆奥行
#define MAP_SIZE 15


//マップを管理するクラス
class Map : public IGameObject
{
	//１区画の情報
	struct
	{
		int height;		//高さ
		int type;		//種類
	} table_[MAP_SIZE][MAP_SIZE];

	//各モデルのＩＤ
	int hModel_[5];

	int editType_;
	int selectType_;

public:
	//コンストラクタ
	Map(IGameObject* parent);

	//デストラクタ
	~Map();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void Edit(int x, int z);


	D3DXVECTOR3 GetMouseCoord(D3DXVECTOR3 mousePosNear,	D3DXVECTOR3 mousePosFar);

	BOOL CommandProcess(HWND hDlg, WPARAM wp);

	void Save();
};